'use strict'

const { ipcRenderer } = require('electron')

document.getElementById('createTotoForm').addEventListener('submit', (e) => {
  // prevent default refresh functionality of forms                                                 
  e.preventDefault()

  console.log(e)            

  // input on the form                          
  const input = e.target[0]

  // send todo to main process
  ipcRenderer.send('add-todo', input.value) 

  // reset input
  input.value = ''
})